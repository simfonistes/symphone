<?php
/**
 * Created by PhpStorm.
 * User: miguelet
 * Date: 14/01/15
 * Time: 19:13
 */


namespace symphone\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class menuAdmin extends Admin
{
        /**
         * @param DatagridMapper $datagridMapper
         */
        protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
                $datagridMapper
                    ->add('id')
                    ->add('nombre')
                    ->add('icono')
              ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
                $listMapper
                    ->add('id')
                    ->add('nombre')
                    ->add('icono')
                    ->add('_action', 'actions', array(
                           'actions' => array(
                                        'show' => array(),
                                        'edit' => array(),
                                        'delete' => array(),
                                    )
                            ))
                ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
                $formMapper


                    ->add('id')
                    ->add('nombre')
                    ->add('icono')
                ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
                $showMapper
                    ->add('id')
                    ->add('nombre')
                    ->add('icono')
                ;
   }
}