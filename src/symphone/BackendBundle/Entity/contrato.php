<?php

namespace symphone\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * contrato
 */
class contrato
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellidos;

    /**
     * @var string
     */
    private $dni;

    /**
     * @var string
     */
    private $tipotarjeta;

    /**
     * @var string
     */
    private $numtarjeta;

    /**
     * @var string
     */
    private $formapago;

    /**
     * @var string
     */
    private $tarifa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return contrato
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     * @return contrato
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return contrato
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set tipotarjeta
     *
     * @param string $tipotarjeta
     * @return contrato
     */
    public function setTipotarjeta($tipotarjeta)
    {
        $this->tipotarjeta = $tipotarjeta;

        return $this;
    }

    /**
     * Get tipotarjeta
     *
     * @return string 
     */
    public function getTipotarjeta()
    {
        return $this->tipotarjeta;
    }

    /**
     * Set numtarjeta
     *
     * @param string $numtarjeta
     * @return contrato
     */
    public function setNumtarjeta($numtarjeta)
    {
        $this->numtarjeta = $numtarjeta;

        return $this;
    }

    /**
     * Get numtarjeta
     *
     * @return string 
     */
    public function getNumtarjeta()
    {
        return $this->numtarjeta;
    }

    /**
     * Set formapago
     *
     * @param string $formapago
     * @return contrato
     */
    public function setFormapago($formapago)
    {
        $this->formapago = $formapago;

        return $this;
    }

    /**
     * Get formapago
     *
     * @return string 
     */
    public function getFormapago()
    {
        return $this->formapago;
    }

    /**
     * Set tarifa
     *
     * @param string $tarifa
     * @return contrato
     */ 
    public function setTarifa($tarifa)
    {
        $this->tarifa = $tarifa;

        return $this;
    }

    /**
     * Get tarifa
     *
     * @return string 
     */
    public function getTarifa()
    {
        return $this->tarifa;
    }
}
