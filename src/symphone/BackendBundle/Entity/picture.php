<?php

namespace symphone\BackendBundle\Entity;


use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;



/**
 * picture
 */
class picture
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $picPath;

    /**
     * @var string
     */
    private $picTitle;

    /**
     * @var string
     */
    private $picAlt;

    /**
     * @var string
     */
    private $picFile;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set picPath
     *
     * @param string $picPath
     * @return picture
     */
    public function setPicPath($picPath)
    {
        $this->picPath = $picPath;

        return $this;
    }

    /**
     * Get picPath
     *
     * @return string 
     */
    public function getPicPath()
    {
        return $this->picPath;
    }

    /**
     * Set picTitle
     *
     * @param string $picTitle
     * @return picture
     */
    public function setPicTitle($picTitle)
    {
        $this->picTitle = $picTitle;

        return $this;
    }

    /**
     * Get picTitle
     *
     * @return string 
     */
    public function getPicTitle()
    {
        return $this->picTitle;
    }

    /**
     * Set picAlt
     *
     * @param string $picAlt
     * @return picture
     */
    public function setPicAlt($picAlt)
    {
        $this->picAlt = $picAlt;

        return $this;
    }

    /**
     * Get picAlt
     *
     * @return string 
     */
    public function getPicAlt()
    {
        return $this->picAlt;
    }

    /**
     * Set picFile
     *
     * @param string $picFile
     * @return picture
     */
    public function setPicFile($picFile)
    {
        $this->picFile = $picFile;

        return $this;
    }

    /**
     * Get picFile
     *
     * @return string 
     */
    public function getPicFile()
    {
        return $this->picFile;
    }
}
