<?php

namespace symphone\BackendBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class backendBundle extends Bundle {

  public function getParent() {
        return 'FOSUserBundle';
    }

}
