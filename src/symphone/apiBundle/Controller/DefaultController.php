<?php

namespace symphone\apiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/*
 * API JSON
 * @author Miguelet
 */

class DefaultController extends Controller {

    /**
     * @return Response
     */
    public function indexAction() {
        $peticion = $this->getRequest();

        $data = json_decode($peticion->getContent());

        if ($data->opcio === "todo") {//if the user has written his name
            $em = $this->getDoctrine()->getManager();
            $menus = $em->getRepository('backendBundle:menu')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->getQuery()
                    ->getArrayResult();
            $em = $this->getDoctrine()->getManager();
            $imagen = $em->getRepository('itemBundle:imagenes')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->getQuery()
                    ->getArrayResult();
            $em = $this->getDoctrine()->getManager();
            $tarifa = $em->getRepository('tarifasBundle:tarifa')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->getQuery()
                    ->getArrayResult();
            /* $em = $this->getDoctrine()->getManager();
              $contacto = $em->getRepository('frontendBundle:contacto')
              ->createQueryBuilder('e')
              ->select('e')
              ->getQuery()
              ->getArrayResult();
             */
            $return1 = array("pintar" => "todo", "menu" => $menus, "imagenes" => $imagen, "tarifa" => $tarifa /* ,"contacto"=> $contacto */);
        } else if ($data->opcio === "menu") {
            //print_r($menus);
            $em = $this->getDoctrine()->getManager();
            $menus = $em->getRepository('backendBundle:menu')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->getQuery()
                    ->getArrayResult();
            //print_r($menus);
            $return1 = array("pintar" => "menu", "menu" => $menus);
        } else if ($data->opcio === "fotos") {

            $em = $this->getDoctrine()->getManager();
            $imagen = $em->getRepository('itemBundle:imagenes')
                    ->createQueryBuilder('e')
                    ->select('e')
                    ->getQuery()
                    ->getArrayResult();

            $return1 = array("pintar" => "imagenes", "imagenes" => $imagen);
        } else if ($data->opcio === "telefono") {

            $em = $this->getDoctrine()->getManager();
            $dql = "select img from itemBundle:smartphone img where img.nombre='$data->nombre'";
            $query = $em->createQuery($dql);
            $phone = $query->getArrayResult();
            $return1 = array("pintar" => "telefono", "telefono" => $phone);
        } else if ($data->opcio == "alta_contrato") {
            /*
              $contrato = new contrato();
              $contrato->setNombre($data->nombre);
              $contrato->setApellidos($data->apellidos);
              $contrato->setDni($data->dni);
              $contrato->setTipotarjeta($data->tipotarjeta);
              $contrato->setNumtarjeta($data->numtarjeta);
              $contrato->setFormadepago($data->formadepago);
              $contrato->setTarifa($data->tarifa);

              $em = $this->getDoctrine()->getManager();
              $em->persist($contrato);
              $em->flush();
             */

            $return1 = array("pintar" => "tarifa_contestacio", "tarifa" => $data->nombre);
        } else if ($data->opcio == "smartphone") {


            $em = $this->getDoctrine()->getManager();
            $smartphone = $em->getRepository('itemBundle:smartphone')
                ->createQueryBuilder('e')
                ->select('e')
                ->getQuery()
                ->getArrayResult();

            $return1 = array("pintar" => "imagenes", "smartphone" => $smartphone);

        //    $return1 = array("pintar" => "tarifa_contestacio", "tarifa" => $data->nombre);
        } else {




            $return1 = array("pintar" => "buit", "imagenes" => $data->opcio);
        }
        $return = json_encode($return1); //jscon encode the array
        // echo "hola";
        return new Response($return, 200, array('Content-Type' => 'application/json')); //make sure it has the correct content type
    }

}
