<?php

namespace symphone\apiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * contacto
 */
class contact
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $phone;

    
    /**
     * @var string
     */
    
    private $mail;

    /**
     * @var string
     */
    private $facebook;

    /**
     * @var string
     */
    private $dondeestamos;

    /**
     * @var string
     */
    private $twitter;

    /**
     * @var string
     */
    private $iconphone;

    /**
     * @var string
     */
    private $iconmail;

    /**
     * @var string
     */
    private $iconfacebook;

    /**
     * @var string
     */
    private $icondondeestamos;

    /**
     * @var string
     */
    private $icontwitter;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param integer $phone
     * @return contacto
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return integer 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return contacto
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set facebook
     *
     * @param string $facebook
     * @return contacto
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string 
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set dondeestamos
     *
     * @param string $dondeestamos
     * @return contacto
     */
    public function setDondeestamos($dondeestamos)
    {
        $this->dondeestamos = $dondeestamos;

        return $this;
    }

    /**
     * Get dondeestamos
     *
     * @return string 
     */
    public function getDondeestamos()
    {
        return $this->dondeestamos;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return contacto
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string 
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set iconphone
     *
     * @param string $icono
     * @return contacto
     */
    public function setIconphone($iconphone)
    {
        $this->iconphone = $iconphone;

        return $this;
    }

    /**
     * Get iconphone
     *
     * @return string 
     */
    public function getIconoPhone()
    {
        return $this->iconophone;
    }

    /**
     * Set iconmail
     *
     * @param string $iconMail
     * @return contacto
     */
    public function setIconMail($iconMail)
    {
        $this->iconmail = $iconMail;

        return $this;
    }

    /**
     * Get iconmail
     *
     * @return string 
     */
    public function getIconMail()
    {
        return $this->iconmail;
    }

    /**
     * Set iconfacebook
     *
     * @param string $iconFacebook
     * @return contacto
     */
    public function setIconFacebook($iconoFacebook)
    {
        $this->iconfacebook = $iconoFacebook;

        return $this;
    }

    /**
     * Get iconfacebook
     *
     * @return string 
     */
    public function getIconFacebook()
    {
        return $this->iconfacebook;
    }

    /**
     * Set icondondeestamos
     *
     * @param string $iconDondeestamos
     * @return contacto
     */
    public function setIconDondeestamos($iconDondeestamos)
    {
        $this->icondondeestamos = $iconDondeestamos;

        return $this;
    }

    /**
     * Get icondondeestamos
     *
     * @return string 
     */
    public function getIconDondeestamos()
    {
        return $this->icondondeestamos;
    }

    /**
     * Set icontwitter
     *
     * @param string $iconTwitter
     * @return contacto
     */
    public function setIconTwitter($iconTwitter)
    {
        $this->icontwitter = $iconTwitter;

        return $this;
    }

    /**
     * Get icontwitter
     *
     * @return string 
     */
    public function getIconTwitter()
    {
        return $this->icontwitter;
    }
}
