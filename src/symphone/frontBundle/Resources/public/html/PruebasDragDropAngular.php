<script src="angular.min.js"></script>

<script src="angular-file-upload-shim.min.js"></script>
<script src="angular-file-upload.min.js"></script>

<div ng-controller="MyCtrl">

    <button ng-file-select ng-model="files" multiple="true">Attach Any File</button>
    <div ng-file-drop ng-model="files" class="drop-box"
         drag-over-class="{accept:'dragover', reject:'dragover-err', delay:100}"
         multiple="true" allow-dir="true" accept="image/*,application/pdf">
        Drop Images or PDFs files here
    </div>
    <div ng-no-file-drop>File Farg/Drop is not supported for this browser</div>

</div>

