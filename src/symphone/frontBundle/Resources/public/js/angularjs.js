//<link href="{{ asset('/bundles/frontend/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet" />
//        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.27/angular.min.js"></script>
//        <script data-require="angular.js@1.3.0" data-semver="1.3.0" src="https://code.angularjs.org/1.3.0/angular.js"></script>
/*
 <script data-require="jquery@*" data-semver="2.0.3" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>
 <script data-require="bootstrap@3.1.1" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
 <script data-require="angular.js@1.3.0" data-semver="1.3.0" src="https://code.angularjs.org/1.3.0/angular.js"></script>
 <link data-require="bootstrap-css@3.1.1" data-semver="3.1.1" rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
 */

/**
 * Created by miguelet on 2/12/2014.
 */
var app = angular.module('myapp', [
    'ngRoute',
    'angularUtils.directives.dirPagination', 'ngCart',
]).config(['$routeProvider', '$locationProvider', 
    function ($routeProvider, $locationProvider) {
        $routeProvider.
            when('/', {
                templateUrl: '/bundles/frontend/html/principal.html',
                controller: 'MyController'
            }).when('/contractes', {
                templateUrl: '/bundles/frontend/html/contractes.html',
                controller: 'MyController'
            }).when('/app_dev.php/contractes', {
                templateUrl: '/bundles/frontend/html/contractes.html',
                controller: 'MyController'
            }).when('/telefono/:phoneName', {
                templateUrl: '/bundles/frontend/html/smartphone.html',
                controller: 'Controller_tlf'
            }).when('/app_dev.php/telefono/:phoneName', {
                templateUrl: '/bundles/frontend/html/smartphone.html',
                controller: 'Controller_tlf'
            }).when('/app_dev.php', {
                templateUrl: '/bundles/frontend/html/principal.html',
                controller: 'MyController'
            }).when('/telefonos', {
                templateUrl: '/bundles/frontend/html/telefonos.html',
                controller: 'Controller_telefonos'
            }).when('/app_dev.php/telefonos', {
                templateUrl: '/bundles/frontend/html/telefonos.html',
                controller: 'Controller_telefonos'
            }).when('/app_dev.php/resumen', {
                templateUrl: '/bundles/frontend/html/cesta.html',
                controller: 'controller_cesta'
            }).when('resumen', {
                templateUrl: '/bundles/frontend/html/cesta.html',
                controller: 'controller_cesta'
            })
        $locationProvider.html5Mode(true)
    }]).config(
    ['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }])

/**
e creat una funcio que desde els controlador la cridem i fem peticio ajax depen el que volem demanar al server
 l'utilitzem ajax(opcio, $scope, $http, $routeParams)
 opcio = a que volem demanar al server
 $scope variable d'angular on emagatzenem tot el q ens ve de la peticio
 $htpp modul per fer la peticio ajax
 $routeParams agafa un parametre de l'url en este cas el nom , l'utilitzem per a bsucar un tlf concret en la BD
 */
function ajax(opcio, $scope, $http, $routeParams) {



    if (opcio === "telefono") {
        var FormData = {'opcio': opcio, 'nombre': $routeParams.phoneName}
        console.log(FormData)

    } else {
        var FormData = {'opcio': opcio}
    };

    var push = function (data) {
        $scope.currentPage = 1;
        $scope.pageSize = 6;
        $scope.meals = [];
        return $scope.meals = data.smartphone;
    };

    var  normal =function(data) {
        $scope.myData = {};
        return $scope.myData = data;
    };

    $http({
        method: 'POST',
        url: 'http://web.miguelet.ovh/api/pintar',
        data: FormData,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
    }).

        success(function (data) {
            if (opcio == "smartphone") {

                push(data)

            } else {
                normal(data);

            }
        }).
        //si la peticion ajax NO fue exitosa se ejecuta error
        error(function (data, status) {
            $scope.data = data || "FALSE";
            $scope.status = status;
            //  console.log('Ha pasado algo inesperado');
        });

}
function Controller_tlf($scope, $http, $routeParams) {
    ajax("telefono", $scope, $http, $routeParams);
    console.log()
}

function MyController($scope, $http) {
    ajax("todo", $scope, $http);
};

function Controller_telefonos($scope, $http) {

    ajax("smartphone", $scope, $http);

    $scope.pageChangeHandler = function (num) {
        console.log('meals page changed to ' + num);
    };
}

function OtherController($scope) {
    $scope.pageChangeHandler = function (num) {

    };
}

  app.controller ('controller_cesta', ['$scope', '$http', 'ngCart', function($scope, $http, ngCart) {
    ngCart.setTaxRate(7.5);
    ngCart.setShipping(2.99);
    console.log (ngCart);
        
    $scope.checkout = function() {
           $scope.summary = ngCart.toObject();
           
         // Post your cart to your resource
         //$http.post('cart/', ngCart.toObject());
    }
}]);

app.controller('Controller_telefonos', Controller_telefonos);
app.controller('OtherController', OtherController);
app.controller("MyController", MyController);
app.controller('Controller_tlf', Controller_tlf)