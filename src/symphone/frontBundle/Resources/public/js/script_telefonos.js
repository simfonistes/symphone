/**
 * Created by miguelet on 13/01/2015.
 */


/*
 * @constructor
 * @param {type} tabla
 * @param {type} url
 * @param {type} txt_search
 * @returns {tabla}
 */
function tabla(txt_search) {



    var this_ = this;

    this.txt_search = txt_search;



    /*
     * this function filter table
     *
     */
    this.filter = function (txt_search, tabla) {
        var phrase = document.getElementById(txt_search);
        var words = phrase.value.toLowerCase().split(" ");
        var table = document.getElementById(tabla);
        var ele;
        for (var r = 1; r < table.rows.length; r++) {
            ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
            var displayStyle = 'none';
            for (var i = 0; i < words.length; i++) {
                if (ele.toLowerCase().indexOf(words[i]) >= 0)
                    displayStyle = '';
                else {
                    displayStyle = 'none';
                    break;
                }
            }
            table.rows[r].style.display = displayStyle;
        }
    };

};
/*
 *
 * @type Function|Function
 * create clousere and new object
 */
var getTelefonos = (function () {
    var telefonos = new tabla("txt_search");
    return function () {
        return telefonos;
    };
})();


/*
 * initzialice
 *
 */

window.onload = getTelefonos();