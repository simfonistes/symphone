<?php

namespace symphone\itemBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('itemBundle:Default:index.html.twig', array('name' => $name));
    }
}
