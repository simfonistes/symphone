<?php

namespace symphone\itemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smartphone
 *
 * @ORM\Table(name="Smartphone")
 * @ORM\Entity
 */
class Smartphone
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="pulgadas", type="string", length=255, nullable=false)
     */
    private $pulgadas;

    /**
     * @var string
     *
     * @ORM\Column(name="resolucion", type="string", length=255, nullable=false)
     */
    private $resolucion;

    /**
     * @var string
     *
     * @ORM\Column(name="marca", type="string", length=255, nullable=false)
     */
    private $marca;

    /**
     * @var string
     *
     * @ORM\Column(name="sistemaoperativo", type="string", length=255, nullable=false)
     */
    private $sistemaoperativo;

    /**
     * @var string
     *
     * @ORM\Column(name="procesador", type="string", length=255, nullable=false)
     */
    private $procesador;

    /**
     * @var string
     *
     * @ORM\Column(name="camarafrontal", type="string", length=255, nullable=false)
     */
    private $camarafrontal;

    /**
     * @var string
     *
     * @ORM\Column(name="camaratrasera", type="string", length=255, nullable=false)
     */
    private $camaratrasera;

    /**
     * @var string
     *
     * @ORM\Column(name="memoriainterna", type="string", length=255, nullable=false)
     */

    private $memoriainterna;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="string", length=255, nullable=false)
     */
    private $precio;

    /**
     * @var string
     *
     * @ORM\Column(name="stock", type="string", length=255, nullable=false)
     */
    private $stock;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ram", type="string", length=255, nullable=false)
     */
    private $ram;

    /**
     * @var string
     *
     * @ORM\Column(name="extra1", type="string", length=255, nullable=false)
     */
    private $extra1;

    /**
     * @var string
     *
     * @ORM\Column(name="extra2", type="string", length=255, nullable=false)
     */
    private $extra2;

    /**
     * @var string
     *
     * @ORM\Column(name="extra3", type="string", length=255, nullable=false)
     */
    private $extra3;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getPulgadas()
    {
        return $this->pulgadas;
    }

    /**
     * @param string $pulgadas
     */
    public function setPulgadas($pulgadas)
    {
        $this->pulgadas = $pulgadas;
    }

    /**
     * @return string
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * @param string $marca
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;
    }

    /**
     * @return string
     */
    public function getResolucion()
    {
        return $this->resolucion;
    }

    /**
     * @param string $resolucion
     */
    public function setResolucion($resolucion)
    {
        $this->resolucion = $resolucion;
    }

    /**
     * @return string
     */
    public function getSistemaOperativo()
    {
        return $this->sistemaoperativo;
    }

    /**
     * @param string $sistemaoperativo
     */
    public function setSistemaOperativo($sistemaoperativo)
    {
        $this->sistemaoperativo = $sistemaoperativo;
    }

    /**
     * @return string
     */
    public function getProcesador()
    {
        return $this->procesador;
    }

    /**
     * @param string $procesador
     */
    public function setProcesador($procesador)
    {
        $this->procesador = $procesador;
    }

    /**
     * @return string
     */
    public function getCamaraFrontal()
    {
        return $this->camarafrontal;
    }

    /**
     * @param string $camarafrontal
     */
    public function setCamaraFrontal($camarafrontal)
    {
        $this->camarafrontal = $camarafrontal;
    }

    /**
     * @return string
     */
    public function getCamaraTrasera()
    {
        return $this->camaratrasera;
    }

    /**
     * @param string $camaratrasera
     */
    public function setCamaraTrasera($camaratrasera)
    {
        $this->camaratrasera = $camaratrasera;
    }

    /**
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param string $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

        /**
     * @return string
     */
    public function getRam()
    {
        return $this->ram;
    }

    /**
     * @param string $ram
     */
    public function setRam($ram)
    {
        $this->ram = $ram;
    }
    
    /**
     * @return string
     */
    public function getMemoriaInterna()
    {
        return $this->memoriainterna;
    }

    /**
     * @param string $memoriainterna
     */
    public function setMemoriaInterna($memoriainterna)
    {
        $this->memoriainterna = $memoriainterna;
    }

    /**
     * @return string
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param string $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return string
     */
    public function getExtra1()
    {
        return $this->extra1;
    }

    /**
     * @param string $extra1
     */
    public function setExtra1($extra1)
    {
        $this->extra1 = $extra1;
    }

    /**
     * @return string
     */
    public function getExtra2()
    {
        return $this->extra2;
    }

    /**
     * @param string $extra2
     */
    public function setExtra2($extra2)
    {
        $this->extra2 = $extra2;
    }

    /**
     * @return string
     */
    public function getExtra3()
    {
        return $this->extra3;
    }

    /**
     * @param string $extra3
     */
    public function setExtra3($extra3)
    {
        $this->extra3 = $extra3;
    }
 
}
