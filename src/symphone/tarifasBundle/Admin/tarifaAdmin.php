<?php

namespace symphone\tarifasBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class tarifaAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
         //   ->add('id')
            ->add('nombretarifas')
            ->add('precio')
            ->add('minutosvoz')
            ->add('datos')
            ->add('establecimiento')
            ->add('detalles')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
         //   ->add('id')
            ->add('nombretarifas')
            ->add('precio')
            ->add('minutosvoz')
            ->add('datos')
            ->add('establecimiento')
            ->add('detalles')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            ->add('nombretarifas')
            ->add('precio')
            ->add('minutosvoz')
            ->add('datos')
            ->add('establecimiento')
            ->add('detalles')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
          //  ->add('id')
            ->add('nombretarifas')
            ->add('precio')
            ->add('minutosvoz')
            ->add('datos')
            ->add('establecimiento')
            ->add('detalles')
        ;
    }

}

