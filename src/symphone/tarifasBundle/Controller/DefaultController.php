<?php

namespace symphone\tarifasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('tarifasBundle:Default:index.html.twig', array('name' => $name));
    }
}
