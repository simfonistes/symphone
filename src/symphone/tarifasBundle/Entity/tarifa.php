<?php

namespace symphone\tarifasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * tarifa
 */
class tarifa
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombretarifas;

    /**
     * @var string
     */
    private $precio;

    /**
     * @var string
     */
    private $minutosvoz;

    /**
     * @var string
     */
    private $datos;

    /**
     * @var string
     */
    private $establecimiento;

    /**
     * @var string
     */
    private $detalles;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombretarifas
     *
     * @param string $nombretarifas
     * @return tarifa
     */
    public function setNombretarifas($nombretarifas)
    {
        $this->nombretarifas = $nombretarifas;

        return $this;
    }

    /**
     * Get nombretarifas
     *
     * @return string 
     */
    public function getNombretarifas()
    {
        return $this->nombretarifas;
    }

    /**
     * Set precio
     *
     * @param string $precio
     * @return tarifa
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return string 
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set minutosvoz
     *
     * @param string $minutosvoz
     * @return tarifa
     */
    public function setMinutosvoz($minutosvoz)
    {
        $this->minutosvoz = $minutosvoz;

        return $this;
    }

    /**
     * Get minutosvoz
     *
     * @return string 
     */
    public function getMinutosvoz()
    {
        return $this->minutosvoz;
    }

    /**
     * Set datos
     *
     * @param string $datos
     * @return tarifa
     */
    public function setDatos($datos)
    {
        $this->datos = $datos;

        return $this;
    }

    /**
     * Get datos
     *
     * @return string 
     */
    public function getDatos()
    {
        return $this->datos;
    }

    /**
     * Set establecimiento
     *
     * @param string $establecimiento
     * @return tarifa
     */
    public function setEstablecimiento($establecimiento)
    {
        $this->establecimiento = $establecimiento;

        return $this;
    }

    /**
     * Get establecimiento
     *
     * @return string 
     */
    public function getEstablecimiento()
    {
        return $this->establecimiento;
    }

    /**
     * Set detalles
     *
     * @param string $detalles
     * @return tarifa
     */
    public function setDetalles($detalles)
    {
        $this->detalles = $detalles;

        return $this;
    }

    /**
     * Get detalles
     *
     * @return string 
     */
    public function getDetalles()
    {
        return $this->detalles;
    }
}
