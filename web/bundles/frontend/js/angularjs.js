 angular.module('myApp', ['angularUtils.directives.dirPagination']);
angular.module('myapp', ['ngRoute'])
    .config(['$routeProvider', '$locationProvider',
        function ($routeProvider, $locationProvider) {
            $routeProvider.
                when('/', {
                    templateUrl: '/bundles/frontend/html/principal.html',
                    controller: 'MyController'
                }).when('/contractes', {
                    templateUrl: '/bundles/frontend/html/contractes.html',
                    controller: 'MyController'
                }).when('/app_dev.php/contractes', {
                    templateUrl: '/bundles/frontend/html/contractes.html',
                    controller: 'MyController'
                }).when('/telefono/:phoneName', {
                    templateUrl: '/bundles/frontend/html/smartphone.html',
                    controller: 'Controller_tlf'
                }).when('/app_dev.php', {
                    templateUrl: '/bundles/frontend/html/principal.html',
                    controller: 'MyController'
                }).when('/telefonos', {
                    templateUrl: '/bundles/frontend/html/telefonos.html',
                    controller: 'Controller_telefonos'
                }).when('/app_dev.php/telefonos', {
                    templateUrl: '/bundles/frontend/html/miguelet.html',
                    controller: 'Controller_telefonos'
                })
            $locationProvider.html5Mode(true)
        }]).config(
    ['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }]).controller("Controller_tlf", function ($scope, $http,$routeParams) {
        $scope.myData = {};
        var opcio = "telefono";
        var FormData = {'opcio': opcio, 'nombre': $routeParams.phoneName}
        $http({
            method: 'POST',
            url: 'http://web.miguelet.ovh/api/pintar',
            data: FormData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
            // si la peticion ajax se realizo con exito se ejecuta success
            success(function (data, status) {
                $scope.myData = data;

                if (data == 'FALSE') {
                    //  $scope.aviso = 'ndaa por aqui';

                } else {
                    console.log("mucho pora qui");
                    
                }
            }).
            //si la peticion ajax NO fue exitosa se ejecuta error
            error(function (data, status) {
                //  $scope.data = data || "FALSE";
                // $scope.status = status;
                console.log('Ha pasado algo inesperado');
            });
    }).controller("Controller_telefonos", function ($scope, $http,$routeParams,filterFilter) {
        $scope.myData = {};
        var opcio = "smartphone";
        $scope.curPage = 0;
        $scope.pageSize = 6;
        var FormData = {'opcio': opcio, 'nombre': $routeParams.phoneName}
        $http({
            method: 'POST',
            url: 'http://web.miguelet.ovh/api/pintar',
            data: FormData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
            // si la peticion ajax se realizo con exito se ejecuta success
            success(function (data, status) {
                $scope.myData = data;
                console.log(data)

                if (data == 'FALSE') {
                    //  $scope.aviso = 'ndaa por aqui';
                    console.log("nada pora qui")
                } else {


                    $scope.noOfPages = Math.ceil(data.smartphone.length/$scope.entryLimit);

                }
            }).
            //si la peticion ajax NO fue exitosa se ejecuta error
            error(function (data, status) {
                //  $scope.data = data || "FALSE";
                // $scope.status = status;
                console.log('Ha pasado algo inesperado');
            });
        $scope.currentPage = 0; //current page
        $scope.maxSize = 100; //pagination max size
        $scope.entryLimit = 6; //max rows for data tabl





          $scope.$watch('buscar.nombre', function(term) {
            // Create $scope.filtered and then calculat $scope.noOfPages, no racing!
           $scope.filtered = filterFilter($scope.myData.smartphone, term);
           $scope.noOfPages = Math.ceil($scope.filtered.length/$scope.entryLimit);
        });



      //  $scope.$watch('pulgadas.pulgadas', function(term) {
            // Create $scope.filtered and then calculat $scope.noOfPages, no racing!
           // $scope.filtered = filterFilter($scope.myData.smartphone, term);
           // $scope.noOfPages = Math.ceil($scope.filtered.length/$scope.entryLimit);
      //  });




    }).controller("MyController", function ($scope, $http) {
        $scope.myData = {};
        var opcio = "todo";
        //var en_js = JSON.stringify(js);
        var FormData = {'opcio': opcio}
        console.log(FormData);
        $http({
            method: 'POST',
            url: 'http://web.miguelet.ovh/api/pintar',
            data: FormData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).
            // si la peticion ajax se realizo con exito se ejecuta success
            success(function (data, status) {
                $scope.myData = data;
                console.log(data)
                if (data == 'FALSE') {

                    console.log("nada pora qui")
                } else {
                    console.log("mucho pora qui");
                }
            }).
            //si la peticion ajax NO fue exitosa se ejecuta error
            error(function (data, status) {
                //  $scope.data = data || "FALSE";
                // $scope.status = status;
                console.log('Ha pasado algo inesperado');
            });
    });
/*
angular.module('myapp').filter('pagination', function()
{
    return function(input, start)
    {
        start = +start;
        return input.slice(start);
    };
});*/
angular.module('myapp').filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});
