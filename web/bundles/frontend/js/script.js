
setTimeout(function(e) {

jQuery(document).ready(function (e) {
    jQuery('.with-hover-text, .regular-link').live('click', function (e) {
       
        e.stopPropagation();
    });
    jQuery(".tarifasPromo1").live('mouseenter', function () {
    
        jQuery(".tarifasPromo1").animate({top: '5px'}, "fast");
        jQuery(".tarifasPromo1").animate({top: '0px'}, "fast");
    })


    jQuery(".tarifasPromo2").live('mouseenter', function () {
        jQuery(".tarifasPromo2").animate({top: '5px'}, "fast");
        jQuery(".tarifasPromo2").animate({top: '0px'}, "fast");
    })

    /***************
     * = Hover text *
     * Hover text for the last slide
     ***************/
    jQuery('.with-hover-text').hover(
        function(e) {
            jQuery(this).css('overflow', 'visible');
            jQuery(this).find('.hover-text')
                .show()
                .css('opacity', 0)
                .delay(200)
                .animate(
                {
                    paddingTop: '25px',
                    opacity: 1
                },
                'fast',
                'linear'
            );
        },
        function(e) {
            var obj = jQuery(this);
            jQuery(this).find('.hover-text')
                .animate(
                {
                    paddingTop: '0',
                    opacity: 0
                },
                'fast',
                'linear',
                function() {
                    jQuery(this).hide();
                    jQuery( obj ).css('overflow', 'hidden');
                }
            );
        }
    );
//si
    var img_loaded = 0;
    var j_images = [];

    /*************************
     * = Controls active menu *
     * Hover text for the last slide
     *************************/

        jQuery('#slide-3 img').each(function (index, element) {
            var time = new Date().getTime();
            var oldHref = jQuery(this).attr('src');
            var myImg = jQuery('<img />').attr('src', oldHref + '?' + time);

            myImg.load(function (e) {
                img_loaded += 1;
                ;
                if (img_loaded == jQuery('#slide-3 img').length) {
                    jQuery(function () {
                        var pause = 10;
                        jQuery(document).scroll(function (e) {
                            delay(function () {

                                    var tops = [];

                                    jQuery('.story').each(function (index, element) {
                                        tops.push(jQuery(element).offset().top - 200);
                                    });

                                    var scroll_top = jQuery(this).scrollTop();

                                    var lis = jQuery('.nav > li');

                                    for (var i = tops.length - 1; i >= 0; i--) {
                                        if (scroll_top >= tops[i]) {
                                            menu_focus(lis[i], i + 1);
                                            break;
                                        }
                                    }
                                },
                                pause);
                        });
                        jQuery(document).scroll();
                    });
                }
            });
        });

});
},1000)

// asta asi
/******************
 * = Gallery width *
 ******************/



setTimeout(function () {
    jQuery(function () {

        var pause = 50; // will only process code within delay(function() { ... }) every 100ms.
        jQuery(window).resize(function () {
            delay(function () {
                    var gallery_images = jQuery('#slide-3 img');

                    var images_per_row = 0;
                    if (gallery_images.length % 2 == 0) {
                        images_per_row = gallery_images.length / 2;
                    } else {
                        images_per_row = gallery_images.length / 2 + 1;
                    }

                    var gallery_width = jQuery('#slide-3 img').width() * jQuery('#slide-3 img').length;
                    gallery_width /= 2;
                    if (jQuery('#slide-3 img').length % 2 != 0) {
                        gallery_width += jQuery('#slide-3 img').width();
                    }

                    jQuery('#slide-3 .row').css('width', gallery_width);

                    var left_pos = jQuery('#slide-3 .row').width() - jQuery('body').width();
                    left_pos /= -2;

                    jQuery('#slide-3 .row').css('left', left_pos);

                },
                pause
            );
        });
        jQuery(window).resize();
    });

}, 2000);


var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function menu_focus(element, i) {
    if (jQuery(element).hasClass('active')) {
        if (i == 6) {
            if (jQuery('.navbar').hasClass('inv') == false)
                return;
        } else {
            return;

        }
    }

    enable_arrows(i);

    if (i == 1 || i == 6)
        jQuery('.navbar').removeClass('inv');
    else
        jQuery('.navbar').addClass('inv');

    jQuery('.nav > li').removeClass('active');
    jQuery(element).addClass('active');

    var icon = jQuery(element).find('.icon');

    var left_pos = icon.offset().left - jQuery('.nav').offset().left;
    var el_width = icon.width() + jQuery(element).find('.text').width() + 10;


    jQuery('.active-menu').stop(false, false).animate(
        {
            left: left_pos,
            width: el_width
        },
        1500,
        'easeInOutQuart'
    );
}

function enable_arrows(dataslide) {
    jQuery('#arrows div').addClass('disabled');
    if (dataslide != 1) {
        jQuery('#arrow-up').removeClass('disabled');
    }
    if (dataslide != 6) {
        jQuery('#arrow-down').removeClass('disabled');
    }
    if (dataslide == 3) {
        jQuery('#arrow-left').removeClass('disabled');
        jQuery('#arrow-right').removeClass('disabled');
    }
}

/*************
 * = Parallax *
 *************/


jQuery(document).ready(function (jQuery) {
    //Cache some variables
    var links = jQuery('.nav').find('li');
    slide = jQuery('.slide');
    button = jQuery('.button');
    mywindow = jQuery(window);
    htmlbody = jQuery('html,body');

    //Create a function that will be passed a slide number and then will scroll to that slide using jquerys animate. The Jquery
    //easing plugin is also used, so we passed in the easing method of 'easeInOutQuint' which is available throught the plugin.
    function goToByScroll(dataslide) {
        var offset_top = (dataslide == 1) ? '0px' : jQuery('.slide[data-slide="' + dataslide + '"]').offset().top;

        htmlbody.stop(false, false).animate({
            scrollTop: offset_top
        }, 1500, 'easeInOutQuart');
    }


    links.live("click", function (e) {
        e.preventDefault();
        dataslide = jQuery(this).attr('data-slide');
        goToByScroll(dataslide);
        jQuery(".nav-collapse").collapse('hide');
    });


    //When the user clicks on the navigation links, get the data-slide attribute value of the link and pass that variable to the goToByScroll function
    //links.click(function (e) {
    //	e.preventDefault();

    //dataslide = jQuery(this).attr('data-slide');
    //goToByScroll(dataslide);
    //jQuery(".nav-collapse").collapse('hide');
    //});

    //When the user clicks on the navigation links, get the data-slide attribute value of the link and pass that variable to the goToByScroll function
    //jQuery('.navigation-slide').click(function (e) {
    //e.preventDefault();
    //dataslide = jQuery(this).attr('data-slide');
    //goToByScroll(dataslide);
    //jQuery(".nav-collapse").collapse('hide');
    //});

    jQuery('.navigation-slide').live('click', function (e) {

        e.preventDefault();

        dataslide = jQuery(this).attr('data-slide');
        console.log(dataslide)
        goToByScroll(dataslide);
        jQuery(".nav-collapse").collapse('hide');
    });


});

/***************
 * = Menu hover *
 ***************/

setTimeout(function () {
    jQuery(document).ready(function (jQuery) {
        //Cache some variables

        var menu_item = jQuery('.nav').find('li');
        menu_item.hover(
            function (e) {

                var icon = jQuery(this).find('.icon');

                var left_pos = icon.offset().left - jQuery('.nav').offset().left;
                var el_width = icon.width() + jQuery(this).find('.text').width() + 10;

                var hover_bar = jQuery('<div class="active-menu special-active-menu"></div>')
                    .css('left', left_pos)
                    .css('width', el_width)
                    .attr('id', 'special-active-menu-' + jQuery(this).data('slide'));

                jQuery('.active-menu').after(hover_bar);
            },
            function (e) {
                jQuery('.special-active-menu').remove();
            }
        );

    });
}, 1000);


/******************
 * = Gallery hover *
 ******************/
jQuery(document).ready(function (jQuery) {
    //Cache some variables
    var images = jQuery('#slide-3 a');

    images.on('hover',
        function (e) {
            var asta = jQuery(this).find('img');
            jQuery('#slide-3 img').not(asta).stop(false, false).animate(
                {
                    opacity: .5
                },
                'fast',
                'linear'
            );
            var zoom = jQuery('<div class="zoom"></div>');
            if (jQuery(this).hasClass('video')) {
                zoom.addClass('video');
            }
            jQuery(this).prepend(zoom);
        },
        function (e) {
            jQuery('#slide-3 img').stop(false, false).animate(
                {
                    opacity: 1
                },
                'fast',
                'linear'
            );
            jQuery('.zoom').remove();
        }
    );
});

/******************
 * = Arrows click  *
 ******************/
jQuery(document).ready(function (jQuery) {
    //Cache some variables
    var arrows = jQuery('#arrows div');

    arrows.live("click", function (e) {
        e.preventDefault();

        if (jQuery(this).hasClass('disabled'))
            return;

        var slide = null;
        var datasheet = jQuery('.nav > li.active').data('slide');
        var offset_top = false;
        var offset_left = false;


        switch (jQuery(this).attr('id')) {
            case 'arrow-up':
                offset_top = (datasheet - 1 == 1) ? '0px' : jQuery('.slide[data-slide="' + (datasheet - 1) + '"]').offset().top;
                break;
            case 'arrow-down':
                offset_top = jQuery('.slide[data-slide="' + (datasheet + 1) + '"]').offset().top;
                break;
            case 'arrow-left':
                offset_left = jQuery('#slide-3 .row').offset().left + 452;
                if (offset_left > 0) {
                    offset_left = '0px';
                }
                break;
            case 'arrow-right':
                offset_left = jQuery('#slide-3 .row').offset().left - 452;
                if (offset_left < jQuery('body').width() - jQuery('#slide-3 .row').width()) {
                    offset_left = jQuery('body').width() - jQuery('#slide-3 .row').width();
                }
                break;
        }

        if (offset_top != false) {
            htmlbody.stop(false, false).animate({
                scrollTop: offset_top

            }, 1500, 'easeInOutQuart');
        }

        if (offset_left != false) {

            if (jQuery('#slide-3 .row').width() != jQuery('body').width()) {
                jQuery('#slide-3 .row').stop(false, false).animate({
                    left: offset_left
                }, 1500, 'easeInOutQuart');
            }
        }
    });
});








/*jQuery(document).ready(function (e) {
    jQuery('.with-hover-text, .regular-link').live('click', function (e) {
       
        e.stopPropagation();
    });
    jQuery(".tarifasPromo1").live('mouseenter', function () {
    
        jQuery(".tarifasPromo1").animate({top: '5px'}, "fast");
        jQuery(".tarifasPromo1").animate({top: '0px'}, "fast");
    })


    jQuery(".tarifasPromo2").live('mouseenter', function () {
        jQuery(".tarifasPromo2").animate({top: '5px'}, "fast");
        jQuery(".tarifasPromo2").animate({top: '0px'}, "fast");
    })

    jQuery('.with-hover-text').hover(
      
            function (e) {
              alert("over")
            jQuery(this).css('overflow', 'visible');
            jQuery(this).find('.hover-text')
                .show()
                .css('opacity', 0)
                .delay(200)
                .animate(
                {
                    paddingTop: '25px',
                    opacity: 1
                },
                'fast',
                'linear'
            );
        },
        function (e) {
            var obj = jQuery(this);
            jQuery(this).find('.hover-text')
                .animate(
                {
                    paddingTop: '0',
                    opacity: 0
                },
                'fast',
                'linear',
                function () {
                    jQuery(this).hide();
                    jQuery(obj).css('overflow', 'hidden');
                }
            );
        }
    );

    var img_loaded = 0;
    var j_images = [];

   

///////////////////////////////////////////////proves//////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////Proves/////////////////////////////////////////////////////////////

jQuery(document).ready(function (jQuery) {
    //Cache some variables
    var images = jQuery('#slide-3 a');

    images.on('hover',
        function (e) {
            var asta = jQuery(this).find('img');
            jQuery('#slide-3 img').not(asta).stop(false, false).animate(
                {
                    opacity: .5
                },
                'fast',
                'linear'
            );
            var zoom = jQuery('<div class="zoom"></div>');
            if (jQuery(this).hasClass('video')) {
                zoom.addClass('video');
            }
            jQuery(this).prepend(zoom);
        },
        function (e) {
            jQuery('#slide-3 img').stop(false, false).animate(
                {
                    opacity: 1
                },
                'fast',
                'linear'
            );
            jQuery('.zoom').remove();
        }
    );
});
});*/