<?php

namespace symphone\apiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use symphone\frontendBundle\Entity\menu;
use symphone\frontendBundle\Entity\imagenes;

/**
 * Description of databaseController
 *
 * @author Torzai
 * @author Miguelet
 */
class databaseController extends Controller {
    //put your code here

    /**
     * @return Response
     */
    public function menu() {

        $em = $this->getDoctrine()->getManager();
        $menus = $em->getRepository('frontendBundle:menu')
                ->createQueryBuilder('e')
                ->select('e')
                ->getQuery()
                ->getArrayResult();

        //print_r($menus);
        return $return1 = array("pintar" => "menu", "info" => $menus);
        //return $return=json_encode($return1);//jscon encode the array
    }

    public function imagenes() {

        $em = $this->getDoctrine()->getManager();
        $imagen = $em->getRepository('frontendBundle:imagenes')
                ->createQueryBuilder('e')
                ->select('e')
                ->getQuery()
                ->getArrayResult();

        //print_r($menus);
        //return $return1 = array("pintar" => "menu", "info" => $menus);
        //return $return=json_encode($return1);//jscon encode the array
    }

}
